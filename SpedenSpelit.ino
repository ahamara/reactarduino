
#include <Arduino.h>
#include <EEPROM.h>
#include "display.h"
int ledLights[4];
int ledSwitch[4];

int currentLed = 0;
int currentPress = 0;
int nextLed;
bool buttonDown = false;

#define ledAmount 4
#define HISCORE_EEPROM 0

int activeLed = 0;
int gameSpeed = 100;
char lastButtonPressed = -1;

int waitLoop = 0;
int flashingCount = 0;
#define PRESS_ORDER_COUNT 257
char pressOrder[PRESS_ORDER_COUNT];


#define STATE_GAME 1
#define STATE_WAIT 2
#define STATE_IDLE 3
#define STATE_END 4
char gameState;

int lastState = -1;
int currentHiscore = 0;
int displayNumber = 0;

void endGame()
{
    Serial.print("currentPress: " );
    Serial.println(currentPress, DEC);
    Serial.print("currentHiscore: " );
    Serial.println(currentHiscore, DEC);
    flashingCount = 10;
    if(currentPress > currentHiscore)
    {
      currentHiscore = currentPress;
      EEPROM.write(HISCORE_EEPROM,currentHiscore );
    }
    gameState = STATE_END;
}

void initGame()
{

    Serial.print("hiscore: ");
  Serial.println(currentHiscore, DEC);
  flashingCount = 0;
  lastButtonPressed = -1;
  randomSeed(analogRead(0));
  currentLed = 0;
  currentPress = 0;
  pressOrder[0] = random() % ledAmount;
  for (int i = 1; i < PRESS_ORDER_COUNT; i++)
  {
    do
    {
      //pressOrder[i] = i%4;
      pressOrder[i] = random() % ledAmount;
    } while (pressOrder[i] == pressOrder[i - 1]);

  }
  gameState = STATE_IDLE;
  gameSpeed = 100;
}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  initGame();
  displayInit();
  currentHiscore = EEPROM.read(HISCORE_EEPROM);

  // Initialize hiscore
  if(currentHiscore >= 0xff)
  {
    currentHiscore = 0;
  }

  for (int i = 0; i < ledAmount; i++)
  {
    ledLights[i] = 6 + i;
    ledSwitch[i] = 2 + i;
    pinMode(ledLights[i], OUTPUT);
    pinMode(ledSwitch[i], INPUT_PULLUP);
  }



}



void loop() {

  // tooks 3 milliseconds
  writeNumber(displayNumber);
  
  
  if(waitLoop > 0)
  {
    Serial.println(waitLoop, DEC);
    waitLoop--;
  }   
  int buttonsDown = 0;
 
  if (lastState != gameState)
  {
    lastState = gameState;
    Serial.println(gameState, DEC);
  }
  if (gameState == STATE_IDLE)
  {
    bool buttonDown[ledAmount];
    for (int i = 0; i < ledAmount; i++)
    {
      buttonDown[i] = false;
    }
    
    for (int i = 0; i < ledAmount; i++)
    {
      digitalWrite(ledLights[i], HIGH);
      int buttonState = digitalRead(ledSwitch[i]);
      if (buttonState == LOW)
      {
        buttonDown[i] = true;
        if(i == 0)
        {
          initGame();
          gameState = STATE_WAIT;
        }

        if(i == 3)
        {
          displayNumber = currentHiscore;
          if(buttonDown[1] && buttonDown[2])
          {
            // Erase highscore if button 1,2,3 are down
            currentHiscore = 0;
            EEPROM.write(HISCORE_EEPROM,currentHiscore );
          }
        }        
      }
      if (buttonState == HIGH)
      { 
        if(i == 3)
        {
          displayNumber = currentPress;
        }
      }
    }
  }

  else if (gameState == STATE_WAIT)
  {
    if(waitLoop <= 0)
    {
      for (int i = 0; i < ledAmount; i++)
      {
        digitalWrite(ledLights[i], LOW);
      }
      waitLoop = 150;
    }
    if(waitLoop == 1)
    {
      nextLed = 100;
      gameState = STATE_GAME;
    }
  }

  
  else if (gameState == STATE_GAME)
  {
  
    activeLed = pressOrder[currentLed];
    for (int i = 0; i < ledAmount; i++)
    {

      // Read INPUT

      int buttonState = digitalRead(ledSwitch[i]);
      if (buttonState == LOW)
      {
        buttonsDown++;
        buttonDown = true;
        lastButtonPressed = i;
      }

      // show led

      if (i == activeLed)
      {
        digitalWrite(ledLights[i], HIGH);
      }
      else
      {
        digitalWrite(ledLights[i], LOW);
      }


    }
    if (buttonsDown == 0)
    {
      if (buttonDown != false)
      {
        // buttonUP
        // Check for doublePress
        if(!(currentPress > 0 && pressOrder[currentPress-1] == lastButtonPressed ))
        {
          if (lastButtonPressed == pressOrder[currentPress])
          {
  
            currentPress++;
            displayNumber = currentPress;
            Serial.print("Now: ");
            Serial.println(lastButtonPressed, DEC);
  
          }
          else
          {
            endGame();
          }
        }

      }
      buttonDown = false;

    }
    //if(buttonDown == false && lastButtonPressed)
    
    nextLed--;
    if (nextLed <= 0)
    {
      nextLed = gameSpeed;
      currentLed++;
      if(currentLed >= PRESS_ORDER_COUNT-1)
        currentLed = PRESS_ORDER_COUNT-1;

      // If player dosent do anything
      if(currentLed > currentPress + 15)
      {
        endGame();
      }
      
      
      if (gameSpeed > 30)
      {
        gameSpeed = gameSpeed * 0.96;
      }
    }

  }

  else if (gameState == STATE_END)
  {
    if(flashingCount > 0)
    {
           
      for (int i = 0; i < ledAmount; i++)
      {
        if (flashingCount % 2 == 0)
        {
          digitalWrite(ledLights[i], LOW);
        }
  
        else
        {
          digitalWrite(ledLights[i], HIGH);
        }
      }

      if(waitLoop == 0)
      {
        flashingCount--;
        waitLoop = 15;
      }
    }
    else
    {
      gameState = STATE_IDLE;
    }
 

  }
  delay(7);



}
