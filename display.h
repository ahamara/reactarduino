
int displayPins[] = {12,23,20,19,18,11,21};
int displayDigits[] = {13, 10,22};


void displayInit()
{
    for(int i = 0; i < 7; i++)
    {
        pinMode(displayPins[i], OUTPUT);
    }
    
    for(int i = 0; i < 3; i++)
    {
        pinMode(displayDigits[i], OUTPUT);
    }
}
void writeDigit(int num)
{
  int binaryNumbers[] = {63, 6, 91, 79, 102, 109, 125, 7, 127, 103};
  if(num == -1)
  {
    for(int i = 0; i < 7; i++)
    {

        digitalWrite(displayPins[i], HIGH);    
    }
    return;
  }
  num = num%10;
  for(int i = 0; i < 7; i++)
  {
    if((binaryNumbers[num]>>i)&1 > 0)
      digitalWrite(displayPins[i], LOW);    
    else
      digitalWrite(displayPins[i], HIGH);     
  }
 
}

void writeNumber(int num)
{
  int writableNum = 0;
  num = num%999;
  digitalWrite(displayDigits[0], LOW);
  digitalWrite(displayDigits[1], LOW);
  digitalWrite(displayDigits[2], LOW);

  digitalWrite(displayDigits[0], HIGH);
  writableNum = num/100;
  if(writableNum == 0)
    writableNum = -1;
  else
  {
    num -= writableNum*100;
  }
  writeDigit(writableNum); 
  delay(1); 
  digitalWrite(displayDigits[0], LOW);
  
  digitalWrite(displayDigits[1], HIGH);
  if(num/10 == 0 && writableNum == -1)
    writableNum = -1;
  else
  {
    writableNum = num/10;
    num -= writableNum*10;
  }
   writeDigit(writableNum); 
  
  delay(1); 
  digitalWrite(displayDigits[1], LOW);

  digitalWrite(displayDigits[2], HIGH);
  writableNum = num;
 
   writeDigit(writableNum); 
  num -= writableNum;
  delay(1); 
  digitalWrite(displayDigits[2], LOW);    
  
}
